//
//  main.m
//  testbit
//
//  Created by Pinn on 6/5/2557 BE.
//  Copyright (c) 2557 Pinn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
