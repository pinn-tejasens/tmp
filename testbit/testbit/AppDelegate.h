//
//  AppDelegate.h
//  testbit
//
//  Created by Pinn on 6/5/2557 BE.
//  Copyright (c) 2557 Pinn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
